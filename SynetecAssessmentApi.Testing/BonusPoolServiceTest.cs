﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SynetecAssessmentApi.Domain;
using SynetecAssessmentApi.Persistence;
using SynetecAssessmentApi.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynetecAssessmentApi.Testing
{
    [TestClass]
    public class BonusPoolServiceTest
    {
        private readonly AppDbContext appDbContext;
        private readonly BonusPoolService bonusPoolService;
        public BonusPoolServiceTest()
        {
            // Ideally a new db context with Moq will be setup. Since a local persistence is used to test already, I have reused it.
            SetupDbContext();
            var dbContextOptionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            dbContextOptionBuilder.UseInMemoryDatabase(databaseName: "TestDb");

            appDbContext = new AppDbContext(dbContextOptionBuilder.Options);
            bonusPoolService = new BonusPoolService(appDbContext);
        }

        public void SetupDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
          .UseInMemoryDatabase(databaseName: "TestDb")
          .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new AppDbContext(options))
            {
                if (context.Employees.Any()) return;
                var employeeList = new List<Employee> {
                new Employee(1, "John Smith", "Accountant (Senior)", 60000, 1),
                new Employee(2, "Janet Jones", "HR Director", 90000, 2),
                new Employee(3, "Robert Rinser", "IT Director", 95000, 3)};

                var departments = new List<Department>
                {
                    new Department(1, "Finance", "The finance department for the company"),
                    new Department(2, "Human Resources", "The Human Resources department for the company"),
                    new Department(3, "IT", "The IT support department for the company"),
                    new Department(4, "Marketing", "The Marketing department for the company")
                };
                context.AddRange(employeeList);
                context.AddRange(departments);
                context.SaveChanges();
            }
        }

        [TestMethod]
        public async Task CalculateBonus_Positive()
        {
            var (status, bonus) = await bonusPoolService.CalculateAsync(10000, 1);
            Assert.AreEqual(2448, bonus.Amount);
            Assert.IsTrue(status);
        }

        [TestMethod]
        public async Task CalculateBonus_EmployeeId_NotExist()
        {
            var (status, bonus) = await bonusPoolService.CalculateAsync(10000, 131231231);
            Assert.IsFalse(status);
            Assert.IsNull(bonus);
        }

        [TestMethod]
        public async Task CalculateBonus_ZeroBonus()
        {
            var (status, bonus) = await bonusPoolService.CalculateAsync(0, 1);
            Assert.AreEqual(0, bonus.Amount);
            Assert.IsTrue(status);
        }

    }
}