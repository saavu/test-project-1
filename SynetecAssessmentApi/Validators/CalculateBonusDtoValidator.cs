﻿using FluentValidation;
using SynetecAssessmentApi.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynetecAssessmentApi.Validators
{
    public class CalculateBonusDtoValidator : AbstractValidator<CalculateBonusDto>
    {
        public CalculateBonusDtoValidator()
        {
            RuleFor(x => x.SelectedEmployeeId).NotNull();
        }
    }
}
