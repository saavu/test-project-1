﻿using Microsoft.AspNetCore.Mvc;
using SynetecAssessmentApi.Dtos;
using SynetecAssessmentApi.Interfaces;
using SynetecAssessmentApi.Services;
using System.Threading.Tasks;

namespace SynetecAssessmentApi.Controllers
{
    [Route("api/[controller]")]
    public class BonusPoolController : Controller, IBonusPoolController
    {
        private readonly IBonusPoolService bonusPoolService;

        public BonusPoolController(IBonusPoolService bonusPoolService)
        {
            this.bonusPoolService = bonusPoolService;
        }

        [HttpPost()]
        public async Task<IActionResult> CalculateBonus([FromBody] CalculateBonusDto request)
        {
            if (!ModelState.IsValid)
            { // checking the rules using fluent validation and returning bad request if it is violated
                return BadRequest();
            }

            // CalculateBonusDto model props has been made nullable since int by default intializes to 0
            var (result,calculateBonusDto) = await bonusPoolService.CalculateAsync(
                request.TotalBonusPoolAmount.Value,
                request.SelectedEmployeeId.Value);
            
            if(result == false)
            {
                // returning not found since its data is non existant in the db
                return NotFound();
            }
            return new JsonResult(calculateBonusDto);
        }
    }
}
