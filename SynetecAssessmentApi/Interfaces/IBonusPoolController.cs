﻿using Microsoft.AspNetCore.Mvc;
using SynetecAssessmentApi.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynetecAssessmentApi.Interfaces
{
    public interface IBonusPoolController
    {
        
        Task<IActionResult> CalculateBonus(CalculateBonusDto request);
    }
}
