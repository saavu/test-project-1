﻿using SynetecAssessmentApi.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynetecAssessmentApi.Interfaces
{
    public interface IBonusPoolService
    {
        Task<(bool, BonusPoolCalculatorResultDto)> CalculateAsync(int bonusPoolAmount, int selectedEmployeeId);
    }
}
